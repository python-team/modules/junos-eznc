junos-eznc (2.7.3-1) unstable; urgency=medium

  * Team upload.
  * Reformat d/control with debputy
  * Bump Standards-Version to 4.7.0
  * New upstream version 2.7.3

 -- Alexandre Detiste <tchet@debian.org>  Sat, 15 Feb 2025 11:30:11 +0100

junos-eznc (2.7.2-2) unstable; urgency=medium

  * Team upload.
  * Add python3-six to build-dependencies (Closes: #1093336)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 17 Jan 2025 20:14:41 +0100

junos-eznc (2.7.2-1) unstable; urgency=medium

  * Team Upload
  * Use GitHub tarball, tests are missing from PyPi
  * New upstream version 2.7.2 (Closes: #1037257)
  * Add dependency on python3-zombie-telnetlib (Closes: #1084573)
  * Use new dh-sequence-python3
  * Set Rules-Requires-Root: no
  * Update dependencies:
    - python3-netaddr
    + python3-nose2
    + python3-ntc-templates
    + python3-pyparsing
    + python3-transitions
    + python3-yamlordereddictloader

 -- Alexandre Detiste <tchet@debian.org>  Wed, 25 Dec 2024 16:45:08 +0100

junos-eznc (2.1.7-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 27 May 2022 18:54:10 +0100

junos-eznc (2.1.7-4) unstable; urgency=medium

  [ Benjamin Drung ]
  * Team upload
  * Use https for homepage URL
  * Switch to debhelper 13

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Lukas Märdian ]
  * Cherry-pick upstream commits (partly) to fix FTBFS with Python 3.8+
    b9bff50 and 74631fc: use new collections.abc module (Closes: #1002187)

 -- Benjamin Drung <bdrung@debian.org>  Wed, 02 Feb 2022 11:48:02 +0100

junos-eznc (2.1.7-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support (Closes: #936771).

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 04 Sep 2019 13:15:03 +0500

junos-eznc (2.1.7-2) unstable; urgency=medium

  * d/patches: patch to correctly detect format when using "insert".

 -- Vincent Bernat <bernat@debian.org>  Mon, 20 Nov 2017 15:39:04 +0100

junos-eznc (2.1.7-1) unstable; urgency=medium

  * New upstream version.
  * d/control: bump Standards-Version.
  * d/control: add Python 3 package.
  * d/control: add build-dependencies on python*-serial.
  * d/patches: add patch to make SSH work with keys and agent.

 -- Vincent Bernat <bernat@debian.org>  Sun, 12 Nov 2017 19:15:05 +0100

junos-eznc (2.0.1-1) unstable; urgency=medium

  * New upstream version.

 -- Vincent Bernat <bernat@debian.org>  Tue, 01 Nov 2016 20:10:34 +0100

junos-eznc (1.3.1-1) unstable; urgency=low

  * Initial package. Closes: #825197.

 -- Vincent Bernat <bernat@debian.org>  Tue, 24 May 2016 15:57:22 +0200
